const express = require('express')
const app = express()
const PORT = process.env.PORT || 3000;

app.get('/', function (req, res) {
  res.send('Hello World')
})

app.listen(PORT, function () {
  console.log(`Example app listening on port ${PORT}...`);
})

app.get('/hello', function (req, res) {
  res.send('Quel est votre nom ?')
})

app.get('/user/:nom', function (req, res) {
  res.send('Bonjour, ' + req.params.nom + ' !')
})
//J'ai tenté un if mais marche pas
/*app.get('/user/:nom', function (req, res) {
  res.send('Bonjour, ' + req.params.nom + ' !')
  if(nom == ''){
    res.send('Quel est votre nom ?')
  }
})*/